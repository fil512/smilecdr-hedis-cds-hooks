# Smile CDR CDS-Hooks HEDIS Demo

### Installation

These instructions assume you have a DSTU3 Smile CDR instance running.  If you don't, create a new
one by unzipping smilecdr*.tar.gz and editing smilecdr/classes/cdr-config-Master.properties, replacing
DSTU3 with R4.

### Build this app

```bash
git clone git@gitlab.com:fil512/smilecdr-hedis-cds-hooks.git
cd smilecdr-hedis-cds-hooks
mvn clean install
cp target/smilecdr-hedis-cds-hooks-1.0-SNAPSHOT.jar ~/smilecdr/customerlib
```

### Configure Smile CDR

1. Add a CDS-Hooks module
    1. Set Spring Context Config Class to com.example.server.TestServerAppCtx
    1. Set FHIR Version to DSTU3
    1. Set the Listener Port to 9321 
    1. Enable Allow Anonymous Access (Normally this endpoint would be secured, but to test the demo it's easier if you have anonymous access)
    1. In Dependencies, set FHIR Storage Module to your DSTU3 Storage module
1. Restart Smile CDR
   
### Load the data

1. Load the contents of src/test/resources/dstu3:
    1. PUT http://localhost:8000/Library/library-fhir-helpers
    1. PUT http://localhost:8000/Library/library-fhir-model-definition
    1. PUT http://localhost:8000/Library/library-pvs-logic
    1. PUT http://localhost:8000/Measure/measure-pvs  
    1. Post patient-bundle.json and hedis-valuesets-bundle.json to POST http://localhost:8000/
    
### Run the CDS-Hooks

Request a list of CDS-Hook Services with

GET http://localhost:9321/cds-services

Calculate Donald's age with:

POST http://localhost:9321/cds-services/calculate-age
```json
{
	"hookInstance": "d1577c69-dfbe-44ad-ba6d-3e05e953b2ea",
	"fhirServer": "http://localhost:8000",
	"hook": "patient-view",
	"context": {
		"userId": "testuser",
		"patientId": "donald"
	}
}
```

Call hedis-pvs CDS-Hook Service with

POST http://localhost:9321/cds-services/hedis-pvs
```json
{
   "hookInstance": "d1577c69-dfbe-44ad-ba6d-3e05e953b2ea",
   "hook": "patient-view",
   "context": {
      "userId": "testuser",
      "patientId": "donald",
      "periodStart": "2020-01-01",
      "periodEnd": "2021-12-31"
   },
   "fhirServer": "http://localhost:8000/"
}
```