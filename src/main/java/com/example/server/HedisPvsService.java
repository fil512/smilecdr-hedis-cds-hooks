package com.example.server;

import ca.cdr.api.cdshooks.CdsService;
import ca.cdr.api.cdshooks.CdsServiceFeedback;
import ca.cdr.api.cdshooks.CdsServicePrefetch;
import ca.cdr.api.cdshooks.json.*;
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import org.hl7.fhir.dstu3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class HedisPvsService {
    public static final String SERVER_BASE = "http://localhost:8000";
    public static final String SERVICE_ID = "hedis-pvs";
    private static final Logger ourLog = LoggerFactory.getLogger(HedisPvsService.class);
    private static final FhirContext ourFhirContext = FhirContext.forDstu3();

    @CdsService(value = SERVICE_ID,
            hook = "patient-view",
            title = "HEDIS PVS",
            description = "Determine whether patient requires pneumococcal vaccine",
            prefetch = {
                    @CdsServicePrefetch(value = "patient", query = "Patient/{{context.patientId}}")            },
            allowAutoFhirClientPrefetch = true
    )
    public CdsServiceResponseJson checkImmunization(CdsServiceRequestJson theCdsRequest) {
        Patient patient = (Patient) theCdsRequest.getPrefetch("patient");

        if (patient == null) {
            throw new InvalidRequestException("'patient' prefetch resource missing from request.");
        }

        CdsServiceResponseJson response = new CdsServiceResponseJson();
        CdsServiceResponseCardJson card = buildCard(response);

        String name = getName(patient);

        MeasureReport pvsMeasureReport = calculateMeasure(theCdsRequest);
        if (inDenominator(pvsMeasureReport)) {
            if (inNumerator(pvsMeasureReport)) {
                card.setSummary(name + " is immunized");
            } else {
                card.setSummary(name + " needs to schedule a pneumococcal vaccination");
                CdsServiceResponseSuggestionJson suggestion = buildSuggestion();
                card.addSuggestion(suggestion);
            }
        } else {
            card.setSummary(name + " is not in the target population for this immunization");
        }

        return response;
    }

    private CdsServiceResponseSuggestionJson buildSuggestion() {
        CdsServiceResponseSuggestionJson suggestion = new CdsServiceResponseSuggestionJson();
        suggestion.setLabel("pneumococcal vaccination");
        CdsServiceResponseSuggestionActionJson action = new CdsServiceResponseSuggestionActionJson();
        action.setType("create");

        ImmunizationRecommendation recommendation = new ImmunizationRecommendation();
        ImmunizationRecommendation.ImmunizationRecommendationRecommendationComponent recommendationPart = recommendation.addRecommendation();
        CodeableConcept vaccineCode = new CodeableConcept()
                .addCoding(
                        new Coding()
                                .setSystem("http://www.ama-assn.org/go/cpt")
                                .setCode("90670")
                                .setDisplay("PCV13 VACCINE IM")
                );
        recommendation.getMeta().setLastUpdated(new Date());
        recommendationPart.setVaccineCode(vaccineCode);
        action.setResource(recommendation);

        action.setDescription("Recommend pneumococcal immunization");
        suggestion.addAction(action);
        return suggestion;
    }

    private CdsServiceResponseCardJson buildCard(CdsServiceResponseJson response) {
        CdsServiceResponseCardJson card = new CdsServiceResponseCardJson();
        card.setIndicator(CdsServiceIndicatorEnum.INFO);
        CdsServiceResponseCardSourceJson source = new CdsServiceResponseCardSourceJson();
        source.setLabel("HEDIS PVS");
        card.setSource(source);
        response.addCard(card);
        return card;
    }

    private boolean inNumerator(MeasureReport thePvsMeasureReport) {
        MeasureReport.MeasureReportGroupComponent pvsChart = thePvsMeasureReport.getGroup().get(0);
        int count = getCountForPopulation(pvsChart, "numerator");
        return count > 0;
    }

    private boolean inDenominator(MeasureReport thePvsMeasureReport) {
        MeasureReport.MeasureReportGroupComponent pvsChart = thePvsMeasureReport.getGroup().get(0);
        int count = getCountForPopulation(pvsChart, "denominator");
        return count > 0;
    }

    private int getCountForPopulation(MeasureReport.MeasureReportGroupComponent thePvsChart, String thePopulationName) {
        Optional<MeasureReport.MeasureReportGroupPopulationComponent> oPop = thePvsChart.getPopulation().stream()
                .filter(pop -> thePopulationName.equals(pop.getCode().getCoding().get(0).getCode()))
                .findFirst();
        if (!oPop.isPresent()) {
            throw new InvalidRequestException("Cannot find " + thePopulationName + " in MeasureReport");
        }
        MeasureReport.MeasureReportGroupPopulationComponent pop = oPop.get();
        return pop.getCount();
    }

    private MeasureReport calculateMeasure(CdsServiceRequestJson theCdsServiceRequest) {
        String fhirServer = theCdsServiceRequest.getFhirServer();
        IGenericClient client = ourFhirContext.newRestfulGenericClient(fhirServer);

        Patient patient = (Patient) theCdsServiceRequest.getPrefetch("patient");
        String patientId = patient.getId();
        String periodStart = theCdsServiceRequest.getContext().getString("periodStart");
        String periodEnd = theCdsServiceRequest.getContext().getString("periodEnd");

        Parameters inParams = new Parameters();
        inParams.addParameter().setName("patient").setValue(new StringType(patientId));
        inParams.addParameter().setName("periodStart").setValue(new DateType(periodStart));
        inParams.addParameter().setName("periodEnd").setValue(new DateType(periodEnd));

        Parameters outParams = client.operation()
                .onInstance("/Measure/measure-pvs")
                .named("$evaluate-measure")
                .withParameters(inParams)
                .execute();

        ourLog.info(ourFhirContext.newJsonParser().setPrettyPrint(true).encodeResourceToString(outParams));
        return (MeasureReport) outParams.getParameterFirstRep().getResource();
    }

    private String getName(Patient thePatient) {
        if (thePatient == null) {
            return null;
        }
        List<HumanName> names = thePatient.getName();
        if (names == null || names.size() == 0 || names.get(0).isEmpty()) {
            return "This patient";
        }
        HumanName nameItem = names.get(0);
        return nameItem.getNameAsSingleString();
    }


    @CdsServiceFeedback(SERVICE_ID)
    public String feedback(CdsServiceFeedbackJson theFeedback) {
        ourLog.info("Feedback for {} received at {}", theFeedback.getCard(), theFeedback.getOutcomeTimestamp());

        return "{\"message\": \"Thank you for your feedback!\"}";
    }
}

