package com.example.server;

import ca.cdr.api.cdshooks.CdsService;
import ca.cdr.api.cdshooks.CdsServiceFeedback;
import ca.cdr.api.cdshooks.CdsServicePrefetch;
import ca.cdr.api.cdshooks.json.*;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class AgeCalculatorCdsService {
	public static final String SERVICE_ID = "calculate-age";
	private static final Logger ourLog = LoggerFactory.getLogger(AgeCalculatorCdsService.class);

	@CdsService(value = SERVICE_ID,
		hook = "patient-view",
		title = "Age",
		description = "Age calculated from patient birthday",
		prefetch = {
			@CdsServicePrefetch(value = "patient", query = "Patient/{{context.patientId}}")
		},
		allowAutoFhirClientPrefetch = true
	)
	public CdsServiceResponseJson calculateAge(CdsServiceRequestJson theCdsRequest) {
		Patient patient = (Patient) theCdsRequest.getPrefetch("patient");

		if (patient == null) {
			throw new InvalidRequestException("'patient' prefetch resource missing from request.");
		}

		Integer ageInYears = calculateAge(patient);
		String name = getName(patient);

		CdsServiceResponseJson response = new CdsServiceResponseJson();
		CdsServiceResponseCardJson card = new CdsServiceResponseCardJson();

		if (ageInYears == null) {
			card.setSummary(name + "'s age is unknown");
		} else {
			card.setSummary(name + " is " + ageInYears + " years old.");
		}

		card.setIndicator(CdsServiceIndicatorEnum.INFO);
		CdsServiceResponseCardSourceJson source = new CdsServiceResponseCardSourceJson();
		source.setLabel("ACME Age Calculator");
		card.setSource(source);
		response.addCard(card);
		return response;
	}

	private String getName(Patient thePatient) {
		if (thePatient == null) {
			return null;
		}
		List<HumanName> names = thePatient.getName();
		if (names == null || names.size() == 0 || names.get(0).isEmpty()) {
			return "This patient";
		}
		HumanName nameItem = names.get(0);
		return nameItem.getNameAsSingleString();
	}

	private Integer calculateAge(Patient thePatient) {
		if (thePatient == null) {
			return null;
		}
		Date birthDate = thePatient.getBirthDate();
		if (birthDate == null) {
			return null;
		}
		LocalDate localBirthDate = birthDate.toInstant()
			.atZone(ZoneId.systemDefault())
			.toLocalDate();

		return Period.between(localBirthDate, Instant.now().atZone(ZoneId.systemDefault()).toLocalDate()).getYears();
	}

	@CdsServiceFeedback("calculate-age")
	public String feedback(CdsServiceFeedbackJson theFeedback) {
		ourLog.info("Feedback for {} received at {}", theFeedback.getCard(), theFeedback.getOutcomeTimestamp());

		return "{\"message\": \"Thank you for your feedback!\"}";
	}
}

