package com.example.server;

import ca.cdr.api.cdshooks.CdsService;
import ca.cdr.api.cdshooks.CdsServicePrefetch;
import ca.cdr.api.cdshooks.json.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DrugInteractionService {
	public static final String SERVICE_ID = "drug-interactions";
	public static final String USER_ID_KEY = "userId";
	public static final String PATIENT_ID_KEY = "patientId";
	public static final String ENCOUNTER_ID_KEY = "encounterId";
	public static final String SELECTIONS_KEY = "selections";
	public static final String DRAFT_ORDERS_KEY = "draftOrders";
	private static final Logger ourLog = LoggerFactory.getLogger(DrugInteractionService.class);

	@CdsService(value = SERVICE_ID,
		hook = "order-select",
		title = "Drug Interaction",
		description = "Determine Drug Interactions",
		prefetch = {
			@CdsServicePrefetch(value = "patient", query = "Patient/{{context.patientId}}"),
			@CdsServicePrefetch(value = "encounter", query = "Encounter/{{context.encounterId}}")
		})
	public CdsServiceResponseJson drugInteractions(CdsServiceRequestJson theCdsRequest) {
		String userId = theCdsRequest.getContext().getString(USER_ID_KEY);
		String patientId = theCdsRequest.getContext().getString(PATIENT_ID_KEY);
		String encounterId = theCdsRequest.getContext().getString(ENCOUNTER_ID_KEY);
		List<String> selections = theCdsRequest.getContext().getArray(SELECTIONS_KEY);
		IBaseResource draftOrders = theCdsRequest.getContext().getResource(DRAFT_ORDERS_KEY);

		CdsServiceResponseJson response = new CdsServiceResponseJson();
		CdsServiceResponseCardJson card = new CdsServiceResponseCardJson();
		ourLog.info("Processing drug interactions for {}", patientId);
		card.setSummary(encounterId + " drug interactions for " + selections);
		card.setIndicator(CdsServiceIndicatorEnum.WARNING);
		CdsServiceResponseCardSourceJson source = new CdsServiceResponseCardSourceJson();
		source.setLabel("Test Source");
		card.setSource(source);
		response.addCard(card);
		return response;
	}
}
