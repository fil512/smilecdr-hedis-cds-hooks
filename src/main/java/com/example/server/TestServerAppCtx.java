package com.example.server;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class TestServerAppCtx {
	public static AgeCalculatorCdsService ourAgeCalculatorCdsService = new AgeCalculatorCdsService();
	public static DrugInteractionService ourDrugInteractionService = new DrugInteractionService();
	public static HedisPvsService ourHedisPvsService = new HedisPvsService();

	/**
	 * This bean is a list of CDS-Hooks classes, each one of which implements one or more
	 * CDS-Hook service and/or service feedback methods.
	 */
	@Bean(name = "cdsServices")
	public List<Object> cdsServices(){
		List<Object> retVal = new ArrayList<>();
		retVal.add(ourAgeCalculatorCdsService);
		retVal.add(ourDrugInteractionService);
		retVal.add(ourHedisPvsService);
		return retVal;
	}
}
