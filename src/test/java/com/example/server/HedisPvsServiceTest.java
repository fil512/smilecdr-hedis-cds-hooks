package com.example.server;

import ca.cdr.api.cdshooks.json.CdsServiceRequestJson;
import ca.cdr.api.cdshooks.json.CdsServiceResponseJson;
import ca.uhn.fhir.context.FhirContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.hl7.fhir.dstu3.model.ImmunizationRecommendation;
import org.hl7.fhir.dstu3.model.Patient;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class HedisPvsServiceTest {
    private static final Logger ourLog = LoggerFactory.getLogger(HedisPvsServiceTest.class);
    private static final FhirContext ourFhirContext = FhirContext.forDstu3();

    @Test
    public void testMeasure() throws JsonProcessingException {
        HedisPvsService service = new HedisPvsService();
        CdsServiceRequestJson request = new CdsServiceRequestJson();
        Patient patient = new Patient();
        patient.setId("Patient/PA1");
        request.addPrefetch("patient", patient);
        CdsServiceResponseJson response = service.checkImmunization(request);
        ImmunizationRecommendation recommendation = (ImmunizationRecommendation) response.getCards().get(0).getSuggestions().get(0).getActions().get(0).getResource();
        ourLog.info(ourFhirContext.newJsonParser().setPrettyPrint(true).encodeResourceToString(recommendation));
    }
}